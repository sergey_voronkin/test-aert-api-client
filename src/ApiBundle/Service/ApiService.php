<?php

namespace ApiBundle\Service;

use GuzzleHttp\Client;

/**
 * Class ApiService
 *
 * @package ApiBundle\Service
 */
class ApiService
{
    /**
     * @var string
     */
    private const API_URI = 'http://api.aerticket.local/';

    /**
     * @var Client
     */
    private $client;

    /**
     * ApiService constructor.
     */
    public function __construct()
    {
        $this->client = new Client(
            [
                'base_uri' => self::API_URI,
            ]
        );
    }

    /**
     * @param string    $departureAirport
     * @param string    $arrivalAirport
     * @param \DateTime $date
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function search(string $departureAirport, string $arrivalAirport, \DateTime $date)
    {
        try {
            return $this->searchRequest($departureAirport, $arrivalAirport, $date->format('Y-m-d'));
        } catch (\Throwable $ex) {
            return ['error' => ['code' => '0.01', 'message' => 'Connection error']];
        }
    }

    /**
     * @param string $departureAirport
     * @param string $arrivalAirport
     * @param string $date
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function searchRequest(string $departureAirport, string $arrivalAirport, string $date)
    {
        $response = $this->client->request(
            'POST',
            'search',
            [
                'auth' => [
                    'api_merchant_1',
                    'api_pass'
                ],
                'json' => [
                    'searchQuery' => [
                        'departureAirport' => $departureAirport,
                        'arrivalAirport'   => $arrivalAirport,
                        'departureDate'    => $date,
                    ],
                ],
            ]
        );

        return json_decode($response->getBody()->getContents(), true);
    }


}