<?php

namespace AppBundle\Controller;

use AppBundle\Form\Model\SearchFilterModel;
use AppBundle\Form\Type\SearchFilterType;
use AppBundle\Services\JsonSerializer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();

        return $this->render(
            '@App/index.html.twig',
            [
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * @return FormInterface
     */
    private function getFilterForm()
    {
        $builder = $this->get('form.factory')->createBuilder(SearchFilterType::class, null);
        $builder->setAction($this->get('router')->generate('app_index_search'));

        return $builder->getForm();
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function searchAction(Request $request)
    {
        $form = $this->getFilterForm();
        $form->handleRequest($request);

        /** @var SearchFilterModel $filter */
        $filter = $form->getData();

        $apiService = $this->get('api.service.api_service');

        $apiResponse = $apiService->search($filter->getDepartureAirport(), $filter->getArrivalAirport(), $filter->getDepartureDate());

        return $this->render('@App/list.html.twig', [
            'results' => $apiResponse,
        ]);

    }
}
