<?php

namespace AppBundle\Form\Type;

use AppBundle\Form\Model\SearchFilterModel;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchFilterType
 *
 * @package AppBundle\Form\Type
 */
class SearchFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'departureAirport',
                TextType::class,
                [
                    'required'   => true,
                    'empty_data' => null,
                    'data'       => 'WRS',
                    'attr'       => [
                        'class'       => 'form-control',
                        'placeholder' => 'WRS',
                    ],
                ]
            )
            ->add(
                'arrivalAirport',
                TextType::class,
                [
                    'required'   => true,
                    'empty_data' => null,
                    'data'       => 'IEV',
                    'attr'       => [
                        'class'       => 'form-control',
                        'placeholder' => 'IEV',
                    ],
                ]
            )
            ->add(
                'departureDate',
                DateType::class,
                [
                    'required'   => true,
                    'empty_data' => null,
                    'widget'     => 'single_text',
                    'data' => new \DateTime('2018-11-14'),
                    'format'     => 'yyyy-MM-dd',
                    'attr'       => [
                        'class'       => 'form-control',
                        'placeholder' => '2018-11-14',
                    ],
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SearchFilterModel::class,
                'method'     => 'POST',
            ]
        );
    }
}
