<?php

namespace AppBundle\Form\Model;

/**
 * Class SearchFilterModel
 *
 * @package StatusCheckBundle\Form\Model
 */
class SearchFilterModel
{
    /**
     * @var string|null
     */
    protected $departureAirport;

    /**
     * @var string|null
     */
    protected $arrivalAirport;

    /**
     * @var \DateTime|null
     */
    protected $departureDate;

    /**
     * @return null|string
     */
    public function getDepartureAirport()
    {
        return $this->departureAirport;
    }

    /**
     * @param null|string $departureAirport
     */
    public function setDepartureAirport($departureAirport)
    {
        $this->departureAirport = $departureAirport;
    }

    /**
     * @return null|string
     */
    public function getArrivalAirport()
    {
        return $this->arrivalAirport;
    }

    /**
     * @param null|string $arrivalAirport
     */
    public function setArrivalAirport($arrivalAirport)
    {
        $this->arrivalAirport = $arrivalAirport;
    }

    /**
     * @return \DateTime|null
     */
    public function getDepartureDate()
    {
        return $this->departureDate;
    }

    /**
     * @param \DateTime|null $departureDate
     */
    public function setDepartureDate($departureDate)
    {
        $this->departureDate = $departureDate;
    }
}
